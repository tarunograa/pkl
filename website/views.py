
from aiohttp import request
from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask_login import login_user, login_required, logout_user, current_user
from . import db
from website.models import Keranjang, Buku, Pinjam, User, Bukupinjam
from sqlalchemy.sql import select
from sqlalchemy import and_, false, or_, text
from datetime import datetime

views = Blueprint('views', __name__)


# def bisaPinjam(username):
#     bnykpinjam = db.session.query(Pinjam.id_pinjam).where(and_(Pinjam.username==username, Pinjam.status!="selesai") ).count()

#     print('banyak pinjam = ',bnykpinjam)
        
#     if(bnykpinjam==1):
#         print(username)
#         checkpinjam = Pinjam.query.where(Pinjam.status!="selesai" and_()).first()
#         print(checkpinjam)
#         checkpinjam = db.session.query(Bukupinjam.id_pinjam).where(Bukupinjam.id_pinjam==checkpinjam.id_pinjam).count()
#         if(checkpinjam==2):
#             return False
#     elif(bnykpinjam==2) :
#         return False
    
#     return True

#Routing ke home page
@views.route('/', methods = ['GET', 'POST'])
def home():
    return render_template("index.html",nilai="",user=current_user, home="active")




#Routing ke menu riwayat pesanan
@views.route('/pesanan', methods = ['GET', 'POST'])
@login_required
def pesanan():
    username = current_user.username
    #ambil berapa peminjaman yang telah dilakukan
    peminjamans = db.session.query(Pinjam).where(Pinjam.username ==username).all()

    #data buku pada tiap peminjaman
    books = []
    for peminjaman in peminjamans:
        buku = db.session.query(Bukupinjam, Buku).join(Buku, Bukupinjam.id_buku==Buku.id_buku).where(Bukupinjam.id_pinjam==peminjaman.id_pinjam).all()
        books.append(buku)

    print(books[1])
    return render_template("pesanan.html", user=current_user, peminjamans=peminjamans, pesananActive="active", books=books)


#Routing ke menu keranjang
@views.route('/cart', methods = ['GET', 'POST'])
@login_required
def cart():
    username = current_user.username


    if request.method == 'POST':

        #ini untuk ambil button apa yang diklkik hapus atau pesan
        button = request.form['button']

        #Kalo user klik checkbox dimasukkan ke sini
        ids = request.form.getlist('option')

    #bagian check mau hapus, pinjam atau hapus
        print(button)
        #Pesan
        if(button=="pesan"):
            #Kirim sama id yang diklik
            return redirect(url_for("pesanbuku.pinjam", id=ids))
            
            # flash("Gagal, peminjaman melebihi batas", category="error")

        #Hapus
        elif (button=="hapus"):
            flash('Koleksi berhasil dihapus', category='success')

            #Itrasi opsi yang di klik
            ids = [int(i) for i in ids]

            #Delete yang dipilih dari database
            Keranjang.query.where(Keranjang.id_buku.in_(ids)).delete()
            db.session.commit()


    #ambil query untuk nampilin isi keranjang
    result = db.session.query(Keranjang, Buku).join(Buku, Keranjang.id_buku==Buku.id_buku).where(Keranjang.username == username).all()

    
    return render_template("cart.html", user=current_user, cart=result, jmlh=len(result), cartActive="active", )




