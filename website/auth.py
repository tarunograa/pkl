from flask import Blueprint, render_template, redirect, url_for, request, flash
from .models import User
from werkzeug.security import  generate_password_hash, check_password_hash
from . import db
from flask_login import login_user, login_required, logout_user, current_user



auth = Blueprint('auth', __name__)

@auth.route('/daftar', methods = ['GET', 'POST'])
def daftar():
    if request.method == 'POST':
        username = request.form['username'].lower()
        nama = request.form['nama']
        noanggota = request.form["noanggota"]
        email = request.form["email"]
        nohp = request.form["nohp"]
        alamat = request.form["alamat"]
        pass1 = request.form["password1"]
        pass2 = request.form["password2"]

        flags = [False, True, False]
                
        if(" " not in username):
            flags[1] = False
            usernamedb = User.query.filter_by(username=username).first()
            if(usernamedb is not  None):
                flags[0] = True
        if(pass1!=pass2):
            flags[2] = True
        
        print(flags[1])
        if True in flags:
            return render_template("signup.html", username=username, nama=nama, email=email, nohp=nohp, noanggota=noanggota, pass1=pass1, pass2=pass2, usernamewarn=flags[0], passwarn=flags[2], spacewarn=flags[1], alamat=alamat)
        else:
            new_user = User(username=username, nama=nama, email=email, nohp=nohp, noanggota=noanggota, password=generate_password_hash(pass1, method='sha256'), alamat=alamat)
            db.session.add(new_user)
            db.session.commit()

            return redirect(url_for('auth.login', regist=True))
    return render_template("signup.html",user=current_user)


@auth.route('/login', methods = ['GET', 'POST'])
def login():
    flag = request.args.get("regist")
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')

        user = User.query.filter_by(username=username).first()
        if user:
            if check_password_hash(user.password, password):
                flash('Login Berhasil!', category='success')
                login_user(user, remember=True)
                return redirect(url_for("views.home"))
            else:
                flash('Password Salah', category='error')
                return render_template("login.html", username=username, user=current_user)
        else:
            flash('Username tidak ditemukan', category='error')
            return render_template("login.html",user=current_user)
        

    # print(flag)
    return render_template("login.html", flag=flag, user=current_user)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('views.home'))
