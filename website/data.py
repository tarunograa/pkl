import requests
import math
import bs4 as bs
from aiohttp import request
from flask import Blueprint, render_template, request, flash
from flask_login import current_user
from . import db
from datetime import datetime

from website.models import Buku, Keranjang

data = Blueprint('data', __name__)

opacaddres = "http://36.91.95.90/inlislite/opac"

def search_html(keyword, ruas, bahan, page="1"):
    find = opacaddres+"/pencarian-sederhana?action=pencarianSederhana&katakunci="+keyword+"&ruas="+ruas+"&bahan="+bahan+"&page="+page+"&per-page=10"
    page = requests.get(find)
    soup = bs.BeautifulSoup(page.content,'html.parser')
    table = soup.find('table')
    pencarian = soup.find_all("div", {"class": "col-sm-12"})        
    return table, pencarian[2].getText().lstrip()
   
def search_detail(id):
    page = requests.get(opacaddres+"/detail-opac?id="+id)
    soup = bs.BeautifulSoup(page.content,'html.parser')
    try:
        soup = soup.find_all("div", {"class": "col-md-9"})
    except:
        return "Pencarian tidak ditemukan"
    return soup

@data.route('/record')
def record():
    key = request.args.get("katakunci")
    katakunci = request.args.get("katakunci").replace(" ","+")
    ruas = request.args.get("ruas").replace(" ","+")
    if ruas != "Judul" :
        key=""
    bahan = request.args.get("bahan").replace(" ","+")
    hal = request.args.get("page")
    if hal==None:
        hal="1"
    try:
        data, pencarian = search_html(katakunci, ruas, bahan, page=hal)
    except:
        return render_template("nobook.html", user=current_user)
    # print(request.url)
    with open('website/templates/table.html', 'w', encoding='utf-8') as f_out:
        f_out.write(data.prettify())
    
    jumlah = pencarian.split()
    # print(jumlah)
    jumlah = jumlah[len(jumlah)-4]
    jumlah = math.ceil((int(jumlah)/10))
    

    return render_template("output.html", cari=pencarian, nilai=key, number= int(hal), maxPages=jumlah, user=current_user, home="active")

@data.route('/detail', methods = ['GET', 'POST'])
def detail():
    id = request.args.get("id")
    
    if request.method == 'POST':
        #check kalo sudah ada di keranjang
        username = current_user.username
        checkid = Keranjang.query.filter_by(id_buku=id).filter_by(username=username).all()
        #check kalo buku sudah pernah dimasukkan ke keranjang
        bukuid = Buku.query.filter_by(id_buku=id).first()

        #Kalo ga kosong, berarti sudah di keranjang
        if (checkid !=[]):
            flash('Buku sudah berada di dalam keranjang', category='warning')
            return render_template("detail.html", user=current_user, home="active")

        #Kalo kosong berarti buku belum pernah masuk keranjang, perlu buat record buku baru
        if (bukuid is None):
            judul = request.form["judul"]
            pengarang = request.form["pengarang"]
            penerbit = request.form["penerbit"]
            desc_fisik = request.form["desc_fisik"]
            nomor_panggil = request.form["nomor_panggil"]
            linkgambar = request.form["imglink"]
            
            #Masukin buku ke record
            new_buku = Buku(id_buku=id, judul_buku=judul, pengarang=pengarang, penerbit=penerbit, desc_fisik=desc_fisik, nomor_panggil=nomor_panggil, linkgambar=linkgambar)
            db.session.add(new_buku)
            db.session.commit()

        #Ambil baris yang paling terakhir di eksekusi berdasarkan username
        result = Keranjang.query.filter_by(username=current_user.username).order_by(Keranjang.time.desc()).first()
        # print(result)
        #kalo ngga kosong berarti digenerate id berdasarkan id terakhir yang masuk
        if (result is not None):
            last_id = result.id_keranjang.replace(username,"") 
            last_id = int(last_id[-1])
            id_keranjang = username+"kj"+str(last_id+1)
        
        #Kalo kosong ya id start dari 1
        else:
            id_keranjang = username+"kj1"
        
        id_buku = id
        now = datetime.now()

        #Masukin buku ke  keranjang
        new_cart = Keranjang(id_keranjang=id_keranjang, id_buku=id_buku, username=username, time=now)
        db.session.add(new_cart)
        db.session.commit()
        flash('Berhasil dimasukkan ke dalam keranjang', category='success')
    
    data = search_detail(id)
    try:
        with open('website/templates/hasildetail.html', 'w', encoding='utf-8') as f_out:
            f_out.write(data[0].prettify())
        
        return render_template("detail.html", user=current_user, home="active")
    
    except:
        return render_template("nobook.html", user=current_user)


# @data.route('/keranjang', methods = ['GET', 'POST'])
# def keranjang():

