from aiohttp import request
from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask_login import login_user, login_required, logout_user, current_user
from . import db
from website.models import Keranjang, Buku, Pinjam, User, Bukupinjam
from sqlalchemy.sql import select
from sqlalchemy import and_, false, or_, text
from datetime import datetime


# importing all required libraries telegram
import telebot
from telethon.sync import TelegramClient
from telethon.tl.types import InputPeerUser, InputPeerChannel
from telethon import TelegramClient, sync, events

pesanbuku = Blueprint('pesanbuku', __name__)



@pesanbuku.route('/pesan', methods = ['GET', 'POST'])
@login_required
def pinjam():

    #ambil id yang dikirim dari page keranjang
    ids=request.args.getlist("id")

    if ids==[]:
        flash("Gagal, pilih item untuk pesanan terlebih dahulu", category="error")
        return redirect(url_for("views.home"))
    

    #Bagian code yang proses pesanan
    if request.method == "POST":
        username = current_user.username
        
        #bagian membuat id pinjam baru
        pinjaman = Pinjam.query.filter_by(username=username).first()
        #jika pinjaman tidak kosong
        if (pinjaman is not None):

            #ambil id peminjaman terakhir ganti username dengan kosong untuk dapat jumlah
            last_id = pinjaman.id_pinjam.replace(username,"") 
            #masih ada bug nanti perbaiki seputar kasus angka id kalo lebih dari 1 digit
            #coba perbaiki dengan [2:]
            last_id = int(last_id[2:])
            #createnew id
            id_pinjam = username+"pj"+str(last_id+1)
        
        #Kalo pinjaman masih kosong ya berarti masih dimulai dari id pertama
        else:
            id_pinjam = username+"pj1"
        
        #ambil data data peminjaman
        alamat = request.form["alamat"]
        status = "diajukan"
        peminjaman = request.form["jenis"]
        biaya = request.form["inputtotalbiaya"]

        # print("liat id pinjam = ", id_pinjam)
        # pinjaman baru, masukkan parameter yang dibutuhkan, id, username, alamat, status, jenis, biaya, sama time
        new_pinjam = Pinjam(id_pinjam=id_pinjam, username=username, alamat=alamat, status=status, peminjaman=peminjaman, biaya=biaya, time=datetime.now())

        #masukkan ke tabel pinjam
        db.session.add(new_pinjam)
        for id in ids:
            #untuk buku yang dipinjam masukkan ke tabel buku pinjam
            print(id)
            new_bukuPinjam = Bukupinjam(id_pinjam=id_pinjam, id_buku=int(id))
            #masukkan ke tabel buku pinjam
            db.session.add(new_bukuPinjam)
        
        #delete buku yang sudah dipinjam dari keranjang
        Keranjang.query.where(Keranjang.id_buku.in_(ids)).delete()
        db.session.commit()
        flash("Permintaan peminjaman berhasil dikirim", category="success")

        #route ke pesnanan kalo udah pinjam
        
        #tinggal kirim ke telegram datanya
        import telepot
        token = '5392022281:AAGhL_dnJnxg2NQd7J9ORSfFmhMJPejHTo4'
        receiver_id = -772168580

        bot = telepot.Bot(token)
        result = db.session.query(Buku).filter(Buku.id_buku.in_(ids)).all()
        judulbuku = []
        nomorpanggil = []
        
        for data in result:
            judulbuku.append(data.judul_buku)
            nomorpanggil.append(data.nomor_panggil)

        databuku=""
        for judul, nomor in zip(judulbuku, nomorpanggil):
            databuku+="\nJudul buku : "+judul+'\nNomor panggil :'+nomor
        bot.sendMessage(receiver_id, 'Pesanan Baru masuk:\nNomor Anggota : '+current_user.noanggota+'\nNama Lengkap '+current_user.nama+'\nAlamat : '+alamat+"\nBuku di "+peminjaman+databuku
        
        )

        return redirect(url_for("views.pesanan"))
        

    #ambil data buku, ini adalah kasus kalo baru datang dari keranjang
    result = db.session.query(Buku).filter(Buku.id_buku.in_(ids)).all()
    #tampilkan pesan
    return render_template("pesan.html", user=current_user, pesan=result, cartActive="active")