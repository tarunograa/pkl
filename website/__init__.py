from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from os import path
from flask_login import LoginManager
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
db = SQLAlchemy()
DB_NAME = "database.db"

# engine = create_engine(f'sqlite:///{DB_NAME}')
# Session = sessionmaker(bind=engine)
# session = Session()

def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'HDHAKFH AKJFH AK HF'
    app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{DB_NAME}'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)

    from .views import views
    from .auth import  auth
    from .data import  data
    from .admin import  admin
    from .pesanbuku import pesanbuku
    app.register_blueprint(views, url_prefix='/')
    app.register_blueprint(auth, url_prefix='/')
    app.register_blueprint(data, url_prefix='/')
    app.register_blueprint(admin, url_prefix='/')
    app.register_blueprint(pesanbuku, url_prefix='/')

    from .models import User, Keranjang
    # @app.route("/static/<path:path>")
    # def static_dir(path):
    #     return send_from_directory("static", path)

    create_database(app)

    login_manager = LoginManager()
    login_manager.login_view = 'views.home'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(username):
        return User.query.get(username)
    return app

def create_database(app):
    if not path.exists('website/'+DB_NAME):
        db.create_all(app=app)
        print("Created database")