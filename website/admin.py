
from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask_login import login_user, login_required, logout_user, current_user
from . import db
from website.models import Keranjang, Buku, Pinjam, User, Bukupinjam
from sqlalchemy.sql import select
from sqlalchemy import and_, false, or_, text
from datetime import datetime

admin = Blueprint('admin', __name__)

@admin.route('/dashboard', methods = ['GET', 'POST'])
def home():
    
    return render_template("admin/dashboard.html")

@admin.route('/daftaruser', methods = ['GET', 'POST'])
def dashboard():
    users = User.query.all()

    print(users)
    return render_template("admin/daftaruser.html", users = users)