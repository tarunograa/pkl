from weakref import WeakValueDictionary
from . import db
from flask_login import UserMixin

class User(db.Model, UserMixin):
    username = db.Column(db.String(150), primary_key=True, unique=True)
    nama = db.Column(db.String(255))
    noanggota = db.Column(db.String(50))
    nohp = db.Column(db.String(20))
    email = db.Column(db.String(150), unique=True)
    password = db.Column(db.String(150))
    alamat = db.Column(db.String(300))
    def get_id(self):
        return (self.username)

class Keranjang(db.Model):
    id_keranjang = db.Column(db.String(20), primary_key=True)
    id_buku = db.Column(db.String(6))
    username = db.Column(db.String(150), db.ForeignKey('user.username'))
    time = db.Column(db.String(20))

class Pinjam(db.Model):
    id_pinjam = db.Column(db.String(50), primary_key=True, unique=True)
    username = db.Column(db.String(150), db.ForeignKey('user.username'))
    alamat = db.Column(db.String(150))
    status = db.Column(db.String(30))
    peminjaman = db.Column(db.String(20))
    biaya = db.Column(db.Integer())
    time = db.Column(db.String(20))
    keterangan = db.Column(db.String(300))

class Buku(db.Model):
    id_buku = db.Column(db.String(20), primary_key=True, unique=True)
    judul_buku = db.Column(db.String(255))
    pengarang = db.Column(db.String(255))
    penerbit = db.Column(db.String(255))
    desc_fisik = db.Column(db.String(255))
    nomor_panggil = db.Column(db.String(255))
    linkgambar = db.Column(db.String(255))

class Bukupinjam(db.Model):
    id_pinjam = db.Column(db.String(50), db.ForeignKey('pinjam.id_pinjam'), primary_key=True)
    id_buku = db.Column(db.String(10), db.ForeignKey('buku.id_buku'))


