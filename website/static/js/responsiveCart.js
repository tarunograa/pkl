function myFunction(x) {
    if (x.matches) { // If media query matches
        img = document.getElementsByClassName("imglarge")
        for (let i = img.length - 1; i >= 0; i--) {
            img[i].remove()
        }
    } else {
        img = document.getElementsByClassName("imgMini")
        for (let i = img.length - 1; i >= 0; i--) {
            img[i].remove()
        }
    }
}

window.addEventListener('resize', myFunction(window.matchMedia("(max-width: 768px)")));

//Disable button if user check 0

checkbox = document.getElementsByClassName("checkbox")
button = document.getElementsByClassName("signup")


function checkedChekbox(checkbox) {
    count = 0
    for (let i = 0; i < checkbox.length; i++) {
        if (checkbox[i].checked) {
            count += 1
        }
    }
    return count
}

for (let i = 0; i < checkbox.length; i++) {
    checkbox[i].addEventListener('change', function () {
        console.log(checkedChekbox(checkbox))
        if (checkbox[i].checked) {
            button[0].disabled = false
            button[1].disabled = false
            button[2].disabled = false
        } else if (checkedChekbox(checkbox) == 0) {
            button[0].disabled = true
            button[1].disabled = true
            button[2].disabled = true
        }
        if (checkedChekbox(checkbox) > 2) {
            button[0].disabled = true
        }
        if (checkedChekbox(checkbox) <= 2 && checkedChekbox(checkbox) > 0) {
            button[0].disabled = false
        }
    });
}

//more / less


chevron = document.getElementsByClassName("fa-chevron-circle-down")

for (let i = 0; i < chevron.length; i++) {
    chevron[i].addEventListener("click", function () {
        chevron[i].classList.toggle("down");
    })
}

//Confirmation button


button[1].addEventListener("click", function (event) {
    if (confirm('Anda yakin ingin melakukan pengambilan buku?')) {
        return true;
    } else {
        event.preventDefault();
        return false;
    }
})
