
button = document.createElement("button")
button.type='submit'
button.classList.add('btn', 'btn-primary','mt-3')
button.innerText="Tambah ke keranjang"
button.value = "Tambah"
button.name = "cart"


cont = document.getElementsByClassName("col-md-9")[0]
cont.classList.remove("col-md-9")

data = document.getElementsByClassName("col-md-10")[0]
data.classList.remove("col-md-10")
data.classList.add("col-md-9")


Image = document.getElementsByClassName("col-md-2")[0]
Image.classList.remove("col-md-2")
Image.classList.add("col-md-3","d-flex","d-md-block","mb-3")

div = document.createElement("form")
div.action = ""
div.method = "post"


p = document.createElement("p")
p.innerText = "Buku hanya dapat ditambahkan ke keranjang apabila buku tersedia dan dapat dipinjam"
p.classList.add("mt-1")

div.appendChild(p)

div.classList.add("ps-3","ps-md-0")

Image.appendChild(div)

//pengecekan apakah buku tersedia atau tidak?
tbody = document.getElementsByTagName("tbody")[1]
trs = tbody.getElementsByTagName("tr")

const td = []
for (let i=0;i<trs.length;i++){
    td.push(trs[i].getElementsByTagName("td")[4].innerText)
}



//tukar posisi foto dan detail saat di desktop
row = document.getElementsByClassName("row")[1]
row.classList.add("d-md-flex","flex-wrap","flex-row-reverse")
document.getElementsByTagName("br")[2].remove()

if(!td.includes("Tersedia\npesan")){
    button.disabled = true
    button.innerText = "Buku Tidak Tersedia"
}

input = document.getElementsByClassName("dataBook")
tbody1 = document.getElementsByTagName("tbody")[0]
trs1 = tbody1.getElementsByTagName("tr")

keterangan = []
isi = []
nopanggil = trs[0].getElementsByTagName("td")[1].innerText
imglink = document.getElementsByTagName("img")[1].src
for(let i=0;i<trs1.length;i++){
    keterangan.push(trs1[i].getElementsByTagName("td")[0].innerText)
    isi.push(trs1[i].getElementsByTagName("td")[1].innerText)
}

for(let i=0;i<keterangan.length;i++){
    if(keterangan[i]=="Judul"){
        input[0].value = isi[i]
    }else if(keterangan[i]=="Pengarang"){
        input[1].value = isi[i]
    }else if(keterangan[i]=="Penerbitan"){
        input[2].value = isi[i]
    }else if(keterangan[i]=="Deskripsi Fisik"){
        input[3].value = isi[i]
    }
}

wrapdatabook = document.getElementsByClassName("wrapdatabook")[0]

// document.getElementsByClassName("wrapdatabook")[0].remove()
input[4].value = nopanggil
input[5].value = imglink
div.appendChild(wrapdatabook)
div.appendChild(button)






